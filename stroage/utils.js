const join  = require('path').join;
const fs    = require('fs');

const TARGET_LON = 51.5144636;
const TARGET_LAT = -0.142571;

const DB_FILE = join(__dirname, 'partners.json');


function readFileContent() {
    return new Promise((resolve, reject) => {
        fs.readFile(DB_FILE, (err, data) => {
            if (err) {
                reject(err)
                return
            }
            resolve(JSON.parse(data));
        })  
    })
}

function getCustomersOfficesInRange(customers, range) {
    const inRangeSet = new Set();

    customers.forEach(customer => {
        if (customer.offices){
            customer.offices.forEach(office => {
                let d = calcOfficeTargeDistance(office);
                if (d < +range) {
                    inRangeSet.add(customer);
                }
            })
        }    
    })
    return [... inRangeSet]
} 

function calcOfficeTargeDistance(office) {
    const lat2 = office.coordinates.split(',')[1]
    const lon2 = office.coordinates.split(',')[0]

    const R = 6371e3; // metres
    const φ1 = TARGET_LAT * Math.PI/180; // φ, λ in radians
    const φ2 = lat2 * Math.PI/180;
    const Δφ = (lat2-TARGET_LAT) * Math.PI/180;
    const Δλ = (lon2-TARGET_LON) * Math.PI/180;

    const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    return (R * c) / 1000 ; // in K.metres
}

function sortCustomers(customers) { 
    return customers.sort((a, b) => a.organization.localeCompare(b.organization))
}

module.exports = {
    readFileContent,
    getCustomersOfficesInRange,
    sortCustomers
}