var express = require('express');
var router = express.Router();

const { readFileContent, getCustomersOfficesInRange, sortCustomers } = require('../stroage/utils');

/* GET all customers listing. */
router.get('/', function(req, res, next) {
  const range = req.query.range;
  readFileContent().then(data => {
    let customers = data;  
    if (!range){
      res.json(sortCustomers(customers))
    } else {
      // Find customers offices in specified range.
      customers = getCustomersOfficesInRange(customers, range);
      res.json(sortCustomers(customers))
    }
   
  }).catch(err => {
    res.send(err)
  });


  
});

module.exports = router;
